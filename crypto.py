from sage.all import *

def fast_power(base, power, modulus):
	assert(power >= 0)
	i = 1
	lib = dict()
        curr = base
        lib[1] = base
	while ((2 ** i) <= power):
		curr = (curr ** 2) % modulus
                lib[(2 ** i)] = curr
                print("value at " + str((2 ** i)) + "= " + str(curr))
                i+=1
	exp = "{0:b}".format(power)
	total = 1
	j = 1
	while (j <= len(exp)):
		curr_char = exp[len(exp) - j]
		if (curr_char == '1'):
			total = (total * lib[2 ** (j-1)]) % modulus
		j+=1
	print(total)
fast_power(7,52,13)
print(7 ** 52 % 13)

def e_add(P,Q,E, A):
    if (P.is_zero()):
        return Q
    if (Q.is_zero()):
        return P
    Px = P.xy()[0]
    Py = P.xy()[1]
    Qx = Q.xy()[0]
    Qy = Q.xy()[1]
    if((Px == Qx) and (Py == -Qy)):
        return E(0)
    else:
        l = (3 * (Px ** 2) + A)/(2 * Py)
        if (P != Q):
            l = (Qy - Py)/(Qx - Px)
        x3 = (l ** 2) - Px - Qx
        y3 = l * (Px - x3) - Py
        return E([x3, y3])

def decompose(n):
    k = 0
    n = n-1
    while ((n % 2) == 0):
        k+=1
        n = n/2
    return k, n

def mrt(k,q):
    n = ((2 ** k)*q) + 1
    iff = True
    for a in range(15):
        if (xgcd(a,n)[0] == 1):
            temp = (a ** q) % n
            if (temp != 1):
                for i in range (k):
                    if (temp == (-1 % n)):
                        print("a=" + str(a) + "\\text{ is not a witness, as } " + str(a) + "^{2^" + str(i) + " \cdot " + str(q) + "} \equiv -1 \mod " +  str(n))
                        iff = False
                    temp = (temp ** 2) % n
                if (iff):
                    iff = True
                    print(str(a) + " is a witness. got past range") 
            else:
                print("a=" +str(a) + "\\text{ is not a witness, as }" + str(a) + "^{"+ str(q) + "} \equiv 1 \mod " + str(n)) 
        else:
            print("gcd fail for " + str(a))

def shanks(g,h,p):
    n = int(1 + math.floor(sqrt(order(g,p))))
    print ("g = " + str(g))
    print ("h = " + str(h))
    print ("p = " + str(p))
    print ("N = " + str(order(g,p)))
    print ("n = " + str(n))
    inv = inverse(g,p)
    print ("g^-1 = " + str(inv))
    u = (inv ** n) % p
    print ("u = " + str(u))
    baby = list()
    giant = list()
    for i in range(n):
        baby.append(g ** (i+1) % p)
    for i in range(n):
        giant.append((h* (u ** (i+1))) % p)
    print("baby =" + str(baby))
    print("giant =" + str(giant))
    ele = list(set(giant).intersection(baby))[0]
    print(set(giant).intersection(baby))
    a = operator.indexOf(baby, ele)
    b = operator.indexOf(giant, ele) 
    print(a+1)
    print(b+1)
    x = ((a+1) + (b+1) * n)
    print((g**x % p) == h)
    return x

def order(g, p):
    order = 1
    curr = g
    while (curr != 1):
        curr *= g
        curr = curr % p
        order += 1
    return order

def dlog(g, p, goal):
    prev = g
    curr = g
    for i in range (p):
        if (curr % p == goal):
            return i+1
        else:
            prev = curr
            curr *= g 
            curr = curr % p


def gcd(a,b, s):
    high = max(a,b)
    low = min(a,b)
    if low == 0:
        s = "+"
        return high
    print (str(high) +" = " + str(high//low) + "\cdot" + str(low) + " + " + str(high%low) + "\\" + "\\")
    s += str(high) + "= "
    g =  gcd(low, high%low, s)
    s += ""

