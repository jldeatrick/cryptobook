# C4IN
**C**rypto **4** **In**struction is an in-progress easy-access API for users to explore cryptographic principles up through elliptic-curve cryptography. The intention of this is to provide those without experience or knowledge of softwares such as MAGMA or SageMath access to the same power, but in the context of one of the most accessible langauges, Python.

Future Plans: convert current SageMath work into a python library and provide brief instructions and documentation for each function. The intent is to treat this documentation as a textbook from which an end user may better understand the crypto principles and protocols involved
